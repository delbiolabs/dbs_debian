#!/bin/bash
# Delbio Termux Boostrapping Script (DBS)
# by Fabio Del Bene <delbio87@gmail.com>
# ispired by Luke Smith (LARBS)
# https://github.com/LukeSmithxyz/LARBS
# License: 


### OPTIONS AND VARIABLES ###

while getopts ":a:r:b:p:h" o; do case "${o}" in
	h) printf "Optional arguments for custom use:\\n  -r: Dotfiles repository (local file or url)\\n  -p: Dependencies and programs csv (local file or url)\\n  -h: Show this message\\n" && exit 1 ;;
	r) dotfilesrepo=${OPTARG} && git ls-remote "$dotfilesrepo" || exit 1 ;;
	b) repobranch=${OPTARG} ;;
	p) progsfile=${OPTARG} ;;
	*) printf "Invalid option: -%s\\n" "$OPTARG" && exit 1 ;;
esac done

[ -z "$dotfilesrepo" ] && dotfilesrepo="https://bitbucket.org/delbiolabs/dotfiles"
[ -z "$progsfile" ] && progsfile="https://bitbucket.org/delbiolabs/dbs_debian/raw/master/progs.csv"
[ -z "$repobranch" ] && repobranch="linux"

### FUNCTIONS ###

error() { clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;}

installpkg(){ apt install -y "$1" >/dev/null 2>&1 ;}

maininstall() { # Installs all needed programs from main repo.
	dialog --title "DBS Installation" --infobox "Installing \`$1\` ($n of $total). $1 $2" 5 70
	installpkg "$1"
	}

installationloop() { \
	([ -f "$progsfile" ] && cat "$progsfile" | sed '/^#/d' > /tmp/progs.csv) || curl -Ls "$progsfile" | sed '/^#/d' > /tmp/progs.csv

	total=$(wc -l < /tmp/progs.csv)
	while IFS=, read -r tag program comment; do
		n=$((n+1))
		echo "$comment" | grep -q "^\".*\"$" && comment="$(echo "$comment" | sed "s/\(^\"\|\"$\)//g")"
		case "$tag" in
			*) maininstall "$program" "$comment" ;;
		esac
	done < /tmp/progs.csv ;
}

### THE ACTUAL SCRIPT ###

### This is how everything happens in an intuitive format and order.
# Check if user is root on Debian distro. Install dialog.
apt install -y "dialog" || error "Are you sure you're running this as the root user, are on an Debian-based distribution and have an internet connection?"

for x in bash curl sudo git make ntp; do
  dialog --title "DBS Installation" --infobox "Installing \`$x\` which is required to install and configure other programs." 5 70
  installpkg "$x"
done

dialog --title "DBS Installation" --infobox "Synchronizing system time to ensure successful and secure installation of software..." 4 70
ntpdate 0.it.pool.ntp.org >/dev/null 2>&1

# The command that does all the installing. Reads the progs.csv file and
# installs each needed program the way required. Be sure to run this only after
# the user has been created and has priviledges to run sudo without a password
# and all build dependencies are installed.
installationloop

# Tap to click
mkdir -p /etc/X11/xorg.conf.d
[ ! -f /etc/X11/xorg.conf.d/40-libinput.conf ] && printf 'Section "InputClass"
        Identifier "libinput touchpad catchall"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Driver "libinput"
	# Enable left mouse button by tapping
	Option "Tapping" "on"
EndSection' > /etc/X11/xorg.conf.d/40-libinput.conf

# Set Rome has localtime
unlink /etc/localtime
ln -s /usr/share/zoneinfo/Europe/Rome /etc/localtime
